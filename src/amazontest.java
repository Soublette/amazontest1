import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class amazontest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\soucar78\\Documents\\JavaSeleniumWork\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		//Scenario: Customer availability to purchase a product to add it to a cart 
		
		//GIVEN the user navigates to www.amazon.com
		driver.get("http://www.amazon.com");
		//AND Searches for �Alexa�
		driver.findElement(By.id("twotabsearchtextbox")).sendKeys("alexa");
		driver.findElement(By.className("nav-input")).click();
		
		//AND navigates to the second page
		//driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[2]/div/span[8]/div/span/div/div/ul/li[3]/a")).click();
		driver.findElement(By.xpath("//li[@class='a-normal']//a[contains(text(),'2')]")).click();
				
		//AND selects the third item
		driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[2]/div/span[4]/div[1]/div[3]/div/span/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/h2/a/span")).click();
		//THEN assert that the item would be available for purchase
		
		String validation = driver.findElement(By.xpath("//span[@class='a-size-medium a-color-success']")).getText();
		
		if (validation.contains("In Stock"))
				
				{
			Assert.assertTrue(true);
			System.out.println("available to purchase");
				}
		else {
			Assert.assertTrue(false);
			System.out.println("unavailable to purchase");
		}
		
	}
}
